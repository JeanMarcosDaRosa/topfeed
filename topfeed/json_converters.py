'''
Created on Jul 23, 2013

@author: Pouria Mellati
'''
import json
from topfeed.domain_classes import Channel, ServerFeedInfo, FavoriteFeedInfo
from topfeed.settings import UserSettings
from topfeed.gui.utils import Color
from topfeed.utils import datetime_to_iso_8601, iso_8601_to_datetime

def json_str_2_UserSettings(jsonStr):
    settings_dict = json.loads(jsonStr)
    channels = set()
    for channel_dict in settings_dict['channels']:
        rgb_dict = channel_dict['color']
        channels.add(Channel(channel_dict['url'], Color(rgb_dict['r'], rgb_dict['g'], rgb_dict['b'], 1), channel_dict['priority']))
    return UserSettings(channels)

def UserSettings_2_json_str(user_settings):
    return json.dumps({'channels': [
        {
            'url': channel.link,
            'color': {
                'r' : channel.color.r,
                'g' : channel.color.g,
                'b' : channel.color.b},
            'priority': channel.priority
        } for channel in user_settings.channels]
    })
    

def json_str_2_ServerFeedInfos(json_str):
    feed_infos = []
    js_channel_objs = json.loads(json_str)
    for channel_obj in js_channel_objs:
        for feed_js in channel_obj['feeds']:
            feed_infos.append(ServerFeedInfo(feed_js['url'], feed_js['upVotes'], feed_js['dnVotes'], feed_js['reads'], feed_js['userVote'], feed_js['hasUserRead']))
    return feed_infos


def bulked_reads_2_json_str(reads):
    ''' The reads param is a dict mapping each channel url to a set of feed urls. '''
    return json.dumps([
        {
         "channel": chan_url,
         "feeds": list(feed_urls)
        }         
    for chan_url, feed_urls in reads.iteritems()])

def FavoriteFeedInfo_2_js_str(fav_feed_inf):
    return json.dumps({
        'url': fav_feed_inf.url,
        'channel': fav_feed_inf.channel_url,
        'title': fav_feed_inf.title,
        'description': fav_feed_inf.description,
        'time': datetime_to_iso_8601(fav_feed_inf.date_time)
    })

def json_2_FavoriteFeedInfo(fav_js):
    return FavoriteFeedInfo(
                url = fav_js['url'],
                channel_url = fav_js['channel'],
                title = fav_js['title'],
                description = fav_js['description'],
                date_time = iso_8601_to_datetime(fav_js['time']))