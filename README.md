#### About
A smart feed reading widget with some social integration.

Users can up-vote or down-vote news items, making them more or less visible for other users. Also simply clicking a news item by one user makes it more visible for others.

The social integration is handled by the server side project, available [here](https://bitbucket.org/pouria_mellati/topfeed-server). The app won't run without the server.

#### Roadmap
* Make app themeable.
* Drop the server side completely, in favor of client-side NLP or machine learning techniques, for identifying interesting items for the user.
